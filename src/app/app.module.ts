import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/modules/material.module';
import { SharedModule } from './shared/shared.module';
import { MainComponent } from './pages/main/main.component';
import { AddEditMenueComponent } from './pages/add-edit-menue/add-edit-menue.component';
import { mainModule } from 'process';
import { MainModule } from './pages/main/main.module';
import { AddEditSubitemComponent } from './pages/add-edit-subitem/add-edit-subitem.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
     BrowserModule,
     AppRoutingModule,
     BrowserAnimationsModule,
     SharedModule,
     MainModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
