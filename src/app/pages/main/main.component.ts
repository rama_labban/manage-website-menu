import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Item, MenuItem } from 'src/app/core/models/menu';
import { AddEditMenueComponent } from '../add-edit-menue/add-edit-menue.component';
import{ groups } from '../../core/data/data';
import { AddEditSubitemComponent } from '../add-edit-subitem/add-edit-subitem.component';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  menuList: MenuItem [] = [];
  constructor(
    public dialog: MatDialog
  ) {
    if(localStorage.length == 0)
    localStorage.setItem('menuList', JSON.stringify(groups) )
    this.menuList = JSON.parse(localStorage.getItem('menuList'));

  }

  ngOnInit(): void {
  }

  dropItem(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    localStorage.setItem('menuList', JSON.stringify(this.menuList) );

  }

  dropGroup(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.menuList, event.previousIndex, event.currentIndex);
    localStorage.setItem('menuList', JSON.stringify(this.menuList) );

  }
  openAddMenuItemDialog(){
    const dialogRef =  this.dialog.open(AddEditMenueComponent, {
      data: { list: this.menuList , comeFrom:'add'},
      width: '300px',
    });
    dialogRef.afterClosed().subscribe((menuItem) => {
      if (menuItem) {
        this.menuList.push(menuItem);
        localStorage.setItem('menuList', JSON.stringify(this.menuList) );

      }
    });
  }
  openEditMenuItemDialog(menuItem: MenuItem){
  const dialogRef =  this.dialog.open(AddEditMenueComponent, {
    data: { menuItem: Object.assign({}, menuItem), comeFrom:'edit' },
    width: '300px',
  });
  dialogRef.afterClosed().subscribe((menuItem) => {
    if (menuItem) {
      let index = this.menuList.findIndex(item => menuItem.id == item.id);
      this.menuList[index] = menuItem;
      localStorage.setItem('menuList', JSON.stringify(this.menuList) );

    }
  });
  }
  deleteMenuItem(item:any){
    this.menuList = this.menuList.filter(menuItem => item.id != menuItem.id)
    localStorage.setItem('menuList', JSON.stringify(this.menuList) );

  }
  openEditSubItemDialog(menuItem: MenuItem, item: Item){
    const dialogRef =  this.dialog.open(AddEditSubitemComponent, {
      data: { item: Object.assign({}, item), comeFrom:'edit' },
      width: '300px',
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
         let index = menuItem.items.findIndex(item => item.id == data.id);
         menuItem.items[index] = data;
         localStorage.setItem('menuList', JSON.stringify(this.menuList) );

      }
    });
  }
  openAddSubItemDialog(menuItem){
    const dialogRef =  this.dialog.open(AddEditSubitemComponent, {
      data: {comeFrom:'add'},
      width: '300px',
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        menuItem.items.push(data);
      }
      localStorage.setItem('menuList', JSON.stringify(this.menuList) );

    });

  }
  deleteItem(menuItem, itemDel){
    menuItem.items = menuItem.items.filter(item => item.id != itemDel.id)
    localStorage.setItem('menuList', JSON.stringify(this.menuList) );
  }
  resetMenu(){
    this.menuList = groups;
    localStorage.removeItem('menuList');
  }
}
