import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { AddEditMenueComponent } from '../add-edit-menue/add-edit-menue.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AddEditSubitemComponent } from '../add-edit-subitem/add-edit-subitem.component';



@NgModule({
  declarations: [
    MainComponent,
    AddEditMenueComponent,
    AddEditSubitemComponent

  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ]
})
export class MainModule { }
