import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from 'src/app/core/models/menu';

@Component({
  selector: 'app-add-edit-subitem',
  templateUrl: './add-edit-subitem.component.html',
  styleUrls: ['./add-edit-subitem.component.scss']
})
export class AddEditSubitemComponent implements OnInit {
  item: Item;
  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    if(this.data.comeFrom == 'add'){
     this.item = new Item();
     }
     else if (this.data.comeFrom == 'edit'){
       this.item = this.data.item;
     }
  }

  ngOnInit(): void {
  }
  addItem(){
    if(this.data.comeFrom == 'add'){
     this.item.id = Number(Math.random().toString().substr(16))

    }

    this.dialogRef.close(this.item);
  }
  closeDialog(){
    this.dialogRef.close();
  }
}
