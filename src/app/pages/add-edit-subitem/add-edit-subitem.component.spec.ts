import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditSubitemComponent } from './add-edit-subitem.component';

describe('AddEditSubitemComponent', () => {
  let component: AddEditSubitemComponent;
  let fixture: ComponentFixture<AddEditSubitemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditSubitemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditSubitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
