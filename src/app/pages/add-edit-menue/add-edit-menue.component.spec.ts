import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditMenueComponent } from './add-edit-menue.component';

describe('AddEditMenueComponent', () => {
  let component: AddEditMenueComponent;
  let fixture: ComponentFixture<AddEditMenueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditMenueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditMenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
