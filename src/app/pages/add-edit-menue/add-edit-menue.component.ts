import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item, MenuItem } from 'src/app/core/models/menu';

@Component({
  selector: 'app-add-edit-menue',
  templateUrl: './add-edit-menue.component.html',
  styleUrls: ['./add-edit-menue.component.scss']
})
export class AddEditMenueComponent implements OnInit {
  menuItem: MenuItem;
  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,

  ) {
    if(this.data.comeFrom == 'add'){
     this.menuItem = new MenuItem();
    }
    else if (this.data.comeFrom == 'edit'){
      this.menuItem = this.data.menuItem;
    }
  }

  ngOnInit(): void {
  }
  addItem(){
    if(this.data.comeFrom == 'add'){
    this.menuItem.id = Number(Math.random().toString().substr(16))
    this.menuItem.items = [];
    }

    this.dialogRef.close(this.menuItem);
  }
  closeDialog(){
    this.dialogRef.close();
  }

}
