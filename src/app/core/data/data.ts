import { MenuItem } from "../models/menu";

 export const groups: MenuItem[] = [
  {
  id: 1,
  title: 'MenuItem 1',
  link:'https://gitlab.com/rama_labban',
  items: [{
    id: 1,
    name: 'Item 1 - MenuItem 1'
  },
  {
    id: 2,
    name: 'Item 2 - MenuItem 1'
  },
  {
    id: 3,
    name: 'Item 3 - MenuItem 1'
  },
  {
    id: 4,
    name: 'Item 4 - MenuItem 1'
  }]
},
{
  id: 2,
  title: 'MenuItem 2',
  link:'https://gitlab.com/rama_labban',
  items: [{
    id: 1,
    name: 'Item 1 - MenuItem 2'
  },
  {
    id: 2,
    name: 'Item 2 - MenuItem 2'
  },
  {
    id: 3,
    name: 'Item 3 - MenuItem 2'
  },
  {
    id: 4,
    name: 'Item 4 - MenuItem 2'
  }]
},
{
  id: 3,
  title: 'MenuItem 3',
  link:'https://gitlab.com/rama_labban',
  items: [{
    id: 1,
    name: 'Item 1 - MenuItem 3'
  },
  {
    id: 2,
    name: 'Item 2 - MenuItem 3'
  },
  {
    id: 3,
    name: 'Item 3 - MenuItem 3'
  },
  {
    id: 4,
    name: 'Item 4 - MenuItem 3'
  }]
}];
