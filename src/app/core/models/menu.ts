export class MenuItem{
id: number;
title: string;
items: Item[];
link: string;
}
export class Item{
  id: number;
  name: string;
}
